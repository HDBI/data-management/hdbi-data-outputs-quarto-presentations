---
title: "HDBI"
subtitle: "![](data/hdbi-logos/HDBI-data-logo.png){width='150'} </br> Data Outputs"
author:
  - name: "[Richard J. Acton](https://orcid.org/0000-0002-2574-9611)"
    orcid: 0000-0002-2574-9611
format: 
  revealjs:
    # theme: dark
    logo: data/hdbi-logos/HDBI-data-logo.png
    footer: "CC BY-SA"
    chalkboard: true
    fullscreen: true
    multiplex: true # for follow along slides
    # navigation-mode: grid
editor: visual
bibliography: references.bib
csl: american-medical-association-brackets.csl
---

```{r setup, include=FALSE, echo=FALSE}
suppressPackageStartupMessages({
  library(jsonlite)
  library(htmltools)
  library(knitr)
  library(markdown)
  library(mime)
  library(rmarkdown)
})
```

# Update

# Data Resource

::: columns
::: {.column width="85%"}
[data-guide.hdbi.org](https://data-guide-hdbi.org) [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8021381.svg)](https://doi.org/10.5281/zenodo.8021381)

![](data/hdbi-logos/HDBI-data-guide-Screenshot.png){width="100%" fig-align="center"}
:::

::: {.column width="15%"}
```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("data-guide.hdbi.org")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/data-guide_hdbi_org.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/data-guide_hdbi_org.svg)
:::
:::

## Data Resource Updates

::: columns
::: {.column width="70%"}
-   Various updates
    -   Data deletion
    -   Data Availability / Integrity
    -   Git tagging
    -   Zenodo DOIs
    -   Updated contribution guidelines
    -   CiTO terms for citations
:::

::: {.column width="30%"}
-   [The Turing Way](https://the-turing-way.netlify.app/index.html) [@community2022]

![](https://the-turing-way.netlify.app/_static/logo-detail-with-text.svg){fig-align="right"}
:::
:::

# Sharing HDBI data

::: columns
::: {.column width="20%"}
FAIR

-   Imaging
-   Single-cell sequencing
-   protocols
:::

::: {.column width="80%"}
![](data/hdbi-logos/FAIR_data_graphic.png)
:::
:::

## Image Data Resource[@williams2017] / BioImage Archive

::: columns
::: {.column width="80%"}
-   BIA: Core archive for image data accompanying any published work
    -   [BIA submission](https://www.ebi.ac.uk/bioimage-archive/submit/)
-   IDR: Added value database
    -   "Reference image datasets, which have value beyond ... supporting an original publication"
    -   Higher bar for metadata annotation
    -   [IDR submission](https://idr.openmicroscopy.org/about/submission.html)
:::

::: {.column width="20%"}
![](data/hdbi-logos/BioImage_Archive_LOGO_LRG_blacktext.png) ![](data/hdbi-logos/idr-logo.svg)
:::
:::

## Human Cell Atlas (HCA)

::: columns
::: {.column width="30%"}
-   Much HDBI single-cell data is be eligible for inclusion here
-   [HCA submission](https://contribute.data.humancellatlas.org/guide)
-   Existing data: [HCA data browser](https://data.humancellatlas.org/explore/projects)
:::

::: {.column width="70%"}
![](data/hdbi-logos/data-suitability.png)
:::
:::

## [Protocols.io](protocols.io)

::: columns
::: {.column width="70%"}
-   Sharing wet-lab protocols
-   Make them citable with a DOI
-   [HDBI workspace](https://www.protocols.io/workspaces/hdbi)

```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("protocols.io/workspaces/hdbi")
svg("data/hdbi-logos/protocols-io_hdbi-workspace.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/protocols-io_hdbi-workspace.svg){width="40%"}
:::

::: {.column width="30%"}
![](data/hdbi-logos/protocols-io_logo_outline.png)
:::
:::

## [Gitlab](https://gitlab.com/HDBI/) ![](data/hdbi-logos/gitlab-logo-500.svg){width="50"} / [Github](https://github.com/HDBI) ![](data/hdbi-logos/github-mark.svg){width="50"} / [Renku](https://renkulab.io/projects/all?searchIn=groups&q=hdbi) ![](data/hdbi-logos/renku_logo_only.svg){width="50"}

-   HDBI groups on all these platforms on which you can share your analysis code

-   Renku for collaboration with shared compute environments

-   Hoping to work with new computational post-docs to ensure that code and compute environments for the analysis of LARRY data is shared on these platforms.

## Zenodo

::: columns
::: {.column width="70%"}
-   [HDBI community on Zenodo](https://zenodo.org/communities/hdbi/)

Finding the HDBI Grant on Zenodo:

"Wellcome Trust: UK Human Developmental Biology Initiative (215116)"

Elsewhere the full code is: 215116/Z/18/Z
:::

::: {.column width="30%"}
![](data/hdbi-logos/zenodo-background.png){fig-align="right" width="100%"}

```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("zenodo.org/communities/hdbi/")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/qr_hdbi-zenodo-community.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/qr_hdbi-zenodo-community.svg)
:::
:::

## HDBI Publications Zotero Group

::: columns
::: {.column width="70%"}
-   Zotero Library of HDBI publications

-   [zotero.org/groups/4832624/hdbi/library](https://www.zotero.org/groups/4832624/hdbi/library)

-   Acknowledge **215116/Z/18/Z** and I should find it

    -   Please let me know about new publications & pre-prints

![](data/hdbi-logos/hdbi-zotero-screenshot.png)
:::

::: {.column width="30%"}
![](https://www.zotero.org/static/images/bs4theme/zotero-logo.1519312231.svg){width="80%"}

```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("zotero.org/groups/4832624/hdbi/library")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/hdbi-zotero-library.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/hdbi-zotero-library.svg)
:::
:::

# Training Symposium

::: columns
::: {.column width="85%"}
Workshops open to HDBI members and their lab-mates

- protocols.io ![](data/hdbi-logos/protocols-io_logo_outline.png){width="50"}
- Renku ![](data/hdbi-logos/renku_logo_only.svg){width="50"}
- Open Microscopy Environment ![](data/hdbi-logos/ome-main-nav-dots-only.svg){width="50"}
- Human Cell Atlas Data Portal ![](data/hdbi-logos/hca-logo.png){width="50"}
- Open, Collaborative, and Reproducible Scientific Workflows

:::

::: {.column width="15%"}
![](data/hdbi-logos/The-Francis-Crick-Institute-logo.png)
*12th September* [Register here](https://www.eventbrite.co.uk/e/hdbi-training-symposium-12-sep-2023-tickets-662238502187) 
```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("eventbrite.co.uk/e/hdbi-training-symposium-12-sep-2023-tickets-662238502187")
svg("data/hdbi-logos/qr-hdbi-training-symposium.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/qr-hdbi-training-symposium.svg)
:::
:::

# Metadata Website and tools {.smaller}

::: columns
::: {.column width="15%"}
![](data/hdbi-logos/hex-isar.png){width="100%"}
:::

::: {.column width="85%"}
-   Building tooling (Current work in progress)
    -   Collecting and editing this information
    -   Building a static site and other formats from it
-   Investigation, Study, Assay model
    -   Structured description of the experiments in HDBI publications
    -   Connecting:
        -   Data
        -   Materials
        -   Protocols / Analysis
:::
:::

# OMERO

::: columns
::: {.column width="70%"}
-   Deploying an HDBI instance of OMERO
-   Currently investigating options for deploying and managing this
-   Space to share HDBI imaging datasets for analysis
-   Staging for deposition in BIA/IDR
    -   Prepare annotations / metadata

::: notes
UCL Microscopy Hub
:::
:::

::: {.column width="30%"}
![](data/hdbi-logos/omero-logomark.png){width="70%"}
:::
:::

# This Presentation

::: columns
::: {.column width="60%"}
[hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/consortium-meeting_2023-06-21.html](https://hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/consortium-meeting_2023-06-21.html)

```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/consortium-meeting_2023-06-21.html")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/consortium-meeting_2023-06-21-slides.svg")
plot(qrcode_obj)
dev.off()
```
:::

::: {.column width="40%"}
![](data/hdbi-logos/consortium-meeting_2023-06-21-slides.svg)
:::
:::

# Questions / Feedback ?

[Richard.Acton\@babraham.ac.uk](mailto:Richard.Acton@babraham.ac.uk)

# References
