---
title: "Data Outputs"
subtitle: "Open, Collaborative, and Reproducible Scientific Workflows"
author:
  - name: "[Richard J. Acton](https://orcid.org/0000-0002-2574-9611)"
    orcid: 0000-0002-2574-9611
format: 
  revealjs:
    theme: dark
    logo: data/hdbi-logos/HDBI-data-logo.png
    footer: "CC BY-SA"
    chalkboard: true
    fullscreen: true
    multiplex: false # for follow along slides
    # navigation-mode: grid
editor: visual
bibliography: references.bib
csl: american-medical-association-brackets.csl
---

```{r setup, include=FALSE, echo=FALSE}
suppressPackageStartupMessages({
  library(jsonlite)
  library(htmltools)
  library(knitr)
  library(markdown)
  library(mime)
  library(rmarkdown)
})
```

# Outline

::: columns

::: {.column width="50%"}
-   Principles
-   Workflow
-   Policies
-   Resources
:::

::: {.column width="50%"}
At each step I present options with varying degrees of impact
:::

:::

::: notes
At each step Options of degree - how ambitious do we want to be with these.
:::

# Principles

What are the principles that we want to try and follow?

# Principles

-   FAIRness
-   Provenance
    -   Biological Materials
    -   Processed Data & Results
-   Reproducibily
    -   At the Bench
    -   In the analysis

# Principles

-   Linked / Semanic Data
    -   Use of ontologies and persistent identifiers
-   Upstreaming
    -   Contributing Outputs back to the scientific ecosystem beyond just papers
-   Incrementalism
    -   Making work public as it develops
    -   Hypotheses as devised, Data/Code/Protocols as generated/developed

::: notes
Upstreaming can take various forms sending data back to HDBR when you use their samples often starts with taking existing community resources e.g. analysis pipelines adapting them to your own needs and commiting to contribute those adaptations back to the original project so future users can benefit from the work Contributing new ontology terms to the ontology if they are missing Contributing to online guides, resources, and documentation

Can be slower to wrap up projects but is ultimately more efficient.
:::

# Workflow

How and when to engage with research data outputs

# Workflow

-   Engaging early with research outputs planning
-   Anticipate costs, bottlenecks, friction
-   Reduce duplicate work
-   Recruit your bioinformaticians early

::: notes
Get you analysts in on the experimental design phase and let them spend the time necessary to properly plan and test their analyses
:::

# Policies

What policies should we adopt and how should we enact them?

# Policies - Scope

-   To whom do they apply?
-   Responsibility for enforcement & Incentives for compliance?

::: notes
If we are going to have policies that apply to members of the consortium then we need criteria for deciding who should follow them.
Only directly funded? to what degree? anyone in a member lab - could to expansive a definition stretch resources Conflicts with institutional policies
:::

# Policies

-   Open Access
    -   Rights retention, pre-prints / self-archiving, Embargos
-   Minimum Requirements / Checklists for sharing
    -   Protocols
    -   Code
    -   Data
-   Deposit on Generation?

::: notes
open access is largely covered by Wellcome policies anyway
:::

# Resources

What resources do we need to implement the policies & workflows necessary to adhere to the principles?

# Resources

-   Roving Research Software Engineer (RSE)
-   Community Management
-   Online Collaboration & Computation
    -   [protocols.io](https://www.protocols.io/)?, [OMERO](https://www.openmicroscopy.org/)?, Slack (or equivalent)
    -   Shared Computational Notebooks ([Renku](renkulab.io) / [Jupyter Hub](https://jupyter.org/hub)), [OSF](https://osf.io/) (open science framework)?
-   Training
    -   What training do people need / want?

::: notes
What should be central and what should be institutional / up to individual groups General Rule: Only have centrally stuff that still adds value if it is ephemeral, or can persist beyond the end of funding without the need for further direct funding/administration.

Community management function / responsibilities - perhaps attach to an existing role ~ workload? 
proactively reach out to people about what they are working on.
networking, communication, bridging gaps, onboarding, posting updates to internal & external comms, attending group meetings and answering questions.
:::

# This Presentation

::: columns
::: {.column width="60%"}
[hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/hdbi2-planning-2024-03-19.html](https://hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/hdbi2-planning-2024-03-19.html)

```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/hdbi2-planning-2024-03-19.html")
svg("data/hdbi-logos/hdbi2-planning-2024-03-19.svg")
plot(qrcode_obj)
dev.off()
```
:::

::: {.column width="40%"}
![](data/hdbi-logos/hdbi2-planning-2024-03-19.svg)
:::
:::

# Questions / Feedback ?

[Richard.Acton\@babraham.ac.uk](mailto:Richard.Acton@babraham.ac.uk)

::: notes
:::

# References


:::notes
-   [Zenodo](https://zenodo.org/)
-   [OSF](https://osf.io/) (open science framework)
-   [IDR](https://idr.openmicroscopy.org/)
-   [OMERO](https://www.openmicroscopy.org/)
-   [HCA](https://www.humancellatlas.org/)
-   [HCA data portal](https://data.humancellatlas.org/)
-   [protocols.io](https://www.protocols.io/)
-   [BIA](https://www.ebi.ac.uk/bioimage-archive/)
-   [BinderHub](https://binderhub.readthedocs.io/en/latest/index.html)
-   [Renku](renkulab.io)
-   [Quarto](https://quarto.org/)
-   [Jupyter-book](https://jupyterbook.org/en/stable/intro.html)
-   automated building: [gitlab CI/CD](https://docs.gitlab.com/ee/ci/) / [github actions](https://docs.github.com/en/actions)
-   Serving the results: [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) / [github pages](https://pages.github.com/)
:::