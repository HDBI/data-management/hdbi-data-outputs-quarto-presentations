---
title: "Update: Learnings from Using Protocols.io"
subtitle: "![](data/hdbi-logos/HDBI-data-logo.png){width='150'} </br> HDBI data outputs"
author:
  - name: "[Richard J. Acton](https://orcid.org/0000-0002-2574-9611)"
    orcid: 0000-0002-2574-9611
format: 
  revealjs:
    theme: dark
    logo: data/hdbi-logos/HDBI-data-logo.png
    footer: "CC BY-SA"
    chalkboard: true
    fullscreen: true
    multiplex: false # for follow along slides
    # navigation-mode: grid
editor: visual
bibliography: references.bib
csl: american-medical-association-brackets.csl
---

```{r setup, include=FALSE, echo=FALSE}
suppressPackageStartupMessages({
  library(jsonlite)
  library(htmltools)
  library(knitr)
  library(markdown)
  library(mime)
  library(rmarkdown)
  library(qrcode)
})
```

# Other Updates

## Data Resource {.smaller}

::: columns
::: {.column width="85%"}
[data-guide.hdbi.org](https://data-guide-hdbi.org) [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8021381.svg)](https://doi.org/10.5281/zenodo.8021381)

-   Recent updates
    -   Image Publication Checklist
    -   Image Data Analysis Publication Checklist
    -   Schmied et al. 2023 @schmied2023

![](data/hdbi-logos/HDBI-data-guide-Screenshot.png){width="100%" fig-align="center"}
:::

::: {.column width="15%"}
```{r, eval=FALSE}
library(qrcode)
qrcode_obj <- qrcode::qr_code("data-guide.hdbi.org")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/data-guide_hdbi_org.svg")
plot(qrcode_obj)
dev.off()
```

![](data/hdbi-logos/data-guide_hdbi_org.svg)
:::
:::

## The Turing Way

::: columns
::: {.column width="70%"}
-   Participating in the TTW book dash this week to contribute to this more widely read resource
-   [The Turing Way](https://the-turing-way.netlify.app/index.html) [@community2022]
:::

::: {.column width="30%"}
![](https://the-turing-way.netlify.app/_static/logo-detail-with-text.svg){fig-align="right"}
:::
:::

# Sharing Protocols

## Why Share Protocols?

::: columns
::: {.column width="60%"}
-   More reproducible methods
-   Space constraints in methods sections
-   Updates/Revisions, feedback
-   Avoid Duplicating work
-   More Citable objects
-   Better/more granular credit
:::

::: {.column width="40%"}
![](data/cos-rpcb-screenshot.png)
:::
:::

## Where to share protocols? {.smaller}
::: columns
::: {.column width="50%"}
-   [Protocol Exchange](https://protocolexchange.researchsquare.com/)
    -   pre-print like, less structured format
-   [Open Science Framework](https://osf.io/)
    -   No protocol specific tooling / structures, some collab
-   [Zenodo](https://zenodo.org/)
    -   No protocol specific tooling / structures
:::
::: {.column width="50%"}
-   [Figshare](https://figshare.com/)
    -   No protocol specific tooling / structures
-   [JoVE](https://www.jove.com/)
    -   Great but a lot of work - Methods publication
-   [Protocols.io](https://www.protocols.io/)
    -   Designed specifically for protocol sharing
    -   More Structured & Machine Readable format
:::
:::

:::notes
- OSF - Centre for open science (no-profit / charity 501(c)(3))
- Zenodo - publically funded by the european commission under OpenAIRE & CERN
- Protocol Exchange and Protocols.io are both owned by Springer Nature
- Figshare owned by Holtzbrinck Publishing Group (owners of Macmillan and the majority of Springer Nature)
- JoVE - independently owned subscription / Hybrid open access with APCs
:::

## Looking at some Examples

LIVE demos! 🤞

- scMTR-seq
- Endometrial Culture System

:::notes
-   To show / mention
    -   PDF
    -   Probably Don't use step cases for sections
    -   Materials in the protocol, are listed automatically in materials
        - Avoid duplication through manual addition
    -   Excessive seperate steps?
        -   Clear order Order 
    -   Runs
    - Sharing
        - Private links
    - Comments
    - Forks
    - versioning
:::

## Adopting a Workflow

Start as you mean to go on

-   By the point you want to share/publish your project it should be(almost) as easy as setting it to public

::: notes
-   Use your data in the same format and with the same identifiers as others would when you publish it
    -   Test the R in FAIR (re-usable)
-   Don't necessarily have to share the data as soon as it's generated to work this way
:::

## Protocols.io a Risk assessment {.smaller}

> Enshittification
> 
> *The phenomenon of online platforms gradually degrading the quality of their services, often by promoting advertisements and sponsored content, in order to increase profits.*

## Protocols.io a Risk assessment {.smaller}

- User Experience is currently good and improving as in growth phase
- Likely to stultify then decline adoption peaks
- Pivot to business users
    - target advertising / sales from scientific supplies to researchers based on materials, methods, & equipment they use
- Once sales funnel is valuable to business users
    - Increase ad rates, charge for favourable search placement
    - Decrease data-portability and other measures to increase switching costs for users
- Primary risk is to collaboration features & API access

:::notes
Example: Mendeley
cooption of data commons from analytics that surveil platform use
Not too bad of a longer term prospect - next little while will likely be as good as it gets
:::

## Mitigation

- Example: OMERO
- AGPL or similar license (Strong copy left with use over network)
    - Dual licensed to Glencoe software who offer a comercial version

# CCCOOCI

::: columns
::: {.column width="85%"}
**Chocolate Chip Cookie Optimisation Open Collaborative Initiative**
:::

::: {.column width="15%"}
![](data/hex-CCCOOCI.png)
:::
:::

:::notes
A project I cooked up (pun intended) to play with protocol.io's & OMERO's APIs for the HDBI training symposium
:::

## Motivation <small>(besides 🍪)</small>

A place to learn about and apply:

-   Working FAIRly (Findable, Accessible, Interoperable, Re-usable)
-   Other Open Science best practices
    -   Open notebooks
    -   Reproducible analyses

## Chunks of CCCOOCI

-   A [Protocol for baking cookies](https://dx.doi.org/10.17504/protocols.io.n92ldmy9nl5b/v1)
    -   Data collected from run records
-   An OMERO server for images of Cookies
    -   [cccooci.pan-narrans.xyz](cccooci.pan-narrans.xyz)
-   A [Renku Project](https://renkulab.io/projects/hdbi/data-management/cccooci)
    -   Run data accessed via the protocols.io API
    -   Image data accessed va the OMERO API
    -   Website with results served from gitlab pages

# Link To This Presentation

::: columns
::: {.column width="60%"}
[hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/prg_lab-meeting_2023-11-13.html](https://hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/prg_lab-meeting_2023-11-13.html)

```{r, eval=FALSE}
qrcode_obj <- qrcode::qr_code("hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/prg_lab-meeting_2023-11-13.html")
# qrcode::generate_svg(,filename = "data/hdbi-logos/data-guide_hdbi_org.svg")
svg("data/hdbi-logos/prg_lab-meeting_2023-11-13_slides.svg")
plot(qrcode_obj)
dev.off()
```
:::

::: {.column width="40%"}
![](data/hdbi-logos/prg_lab-meeting_2023-11-13.html_slides.svg)
:::
:::

# [ReproducibiliTea Journal Club](https://babraham-reproducibilitea-journal-club.gitlab.io/babraham-reproducibilitea-journal-club/about.html)

Monday November 20th, 15:30, Alec Bangham Room

Trying a new variant on the format - No preparation needed - Pick a 1 or 2 figures to work on during the session ([vote](https://www.rcv123.org/ballot/1Yr2HFGvu3jL8LmjHBdNvA))

Covering: [Single-cell transcriptome profiling of the human developing spinal cord reveals a conserved genetic programme with human-specific features](https://doi.org/10.1242/dev.199711) @rayon2021 DOI: [10.1242/dev.199711](https://doi.org/10.1242/dev.199711)

Join the Teams Team: Search, follow this [link](https://teams.microsoft.com/l/team/19%3a-XhATo3omWRNimNSpz37jiY8uyp3ERenJYtE46mZf9E1%40thread.tacv2/conversations?groupId=a949c068-c193-445e-b805-cf0d81ca622d&tenantId=de1147e5-c0dd-4a46-9d77-c8f4e6b43d00) or use code: `lhyqauj`

# Questions / Feedback ?

[Richard.Acton\@babraham.ac.uk](mailto:Richard.Acton@babraham.ac.uk)

::: notes
:::

# References
