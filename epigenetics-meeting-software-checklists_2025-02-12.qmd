---
title: "Research Software Sharing, Publication, & Distribution Checklists"
subtitle: " "
author:
  - name: "[Richard J. Acton](https://orcid.org/0000-0002-2574-9611)"
    orcid: 0000-0002-2574-9611
format: 
  revealjs:
    width: 1920
    height: 1080
    margin: 0.05
    theme: dark
    logo: data/hdbi-logos/HDBI-data-logo.png
    footer: "CC BY-SA"
    chalkboard: true
    fullscreen: true
    multiplex: false # for follow along slides
    # navigation-mode: grid
    mainfont: '"Poppins", sans-serif'
    include-in-header:
      text: |
        <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap')
        </style>
editor: visual
bibliography: references.bib
csl: american-medical-association-brackets.csl
---

```{r setup, include=FALSE, echo=FALSE}
suppressPackageStartupMessages({
  library(jsonlite)
  library(htmltools)
  library(knitr)
  library(markdown)
  library(mime)
  library(rmarkdown)
})
```

# Importance of Software to Research {.smaller}

::: columns

::: {.column width="50%"}
**Essentially all modern research makes use of software in some way**

````r
print("Software Eats The World")
````

Many research projects include some software as a research output:

- Not limited to software tools for use by others 
- May only be 'one-off' scripts specific to your analysis
	- These are effectively a part of the (extended) methods of your work

:::
::: {.column width="50%"}

**What are the best practices for treating software as a research output?**

Inspired by Community-developed checklists for publishing images and image analyses [@schmied2023].

> for scientists wishing to publish obtained images and image-analysis
> results, there are currently no unified guidelines for best practices

The same applies to research software outputs

:::
:::

# Limitations of existing resources

::: columns
::: {.column width="50%"}

- [The FAIR Principles for research software](https://doi.org/10.1038/s41597-022-01710-x)
- [ELIXIR Software Management Plan for Life Sciences](https://doi.org/10.37044/osf.io/k8znb)
- [Software Sustainability Institute Checklist for a Software Management Plan](https://doi.org/10.5281/ZENODO.2159713) 

all excellent resources

:::
::: {.column width="50%"}

**However**

- Too developer focused
  - Advice primarily for software packages
- Insufficiently aspirational
  - Long boring PDFs that aren't very fun 🥱

:::
:::

## What is Computational Reproducibility? {.smaller}

::: columns

::: {.column width="50%"}
> The ability to precisely describe and recreate an exactly logically equivalent compute environment in which some code was executed

**Why does it matter?**

- Focus on the biological or analytical sources of variation not technical ones
- Not losing time to 'dependency hell' & 'it worked on my machine'
- Transparency of process, understanding how we came to our conclusions

![Figure from [The Turing Way](https://book.the-turing-way.org/) (fig. 5), Used under a [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en) licence](https://the-turing-way.netlify.app/_images/reproducible-matrix.jpg){fig-alt="Grid with the characteristics of: Reproducible; same data, same analysis. Replicable; different data, same analysis. Robust; same data, different analysis. And generalisable; different data, different analysis; Research"}

:::

::: {.column width="50%"}

![Reproduced from: 'Toward practical transparent verifiable and long-term reproducible research using Guix' @vallet2022 Figure 1](data/vallet2022_fig1.png)

- Complex problem in modern software stacks
    - The R 4.1.1 interpreter requires 304 binaries from a dependency graph with 935 edges [@vallet2022]
    - That's before we get to any R packages
- Not well addressed by currently popular tools for managing software environments

:::
:::

**How to deal with the problem of helping others reproduce our computations?**

:::notes

- A challenge of specialisation in team science: you may not know what is needed to maximise the reproducibility of work outside your speciality

- You analysis does not become another potential source of technical variation when attempting to replicate a finding if it is reproducible
- knowing how you code differs from another analysis means to know to what differences a finding is robust

**Terminology**

:::{style="font-size: 60%;"}
- git - a version control (aka source control) tool, keeps track to versions of text, facilitates collaboration
- git forge - e.g. GitHub a collaboration platform built on top of git
- Source code - code written by humans
- Binary - compiled code, source is compiled to a binary that can run on a computer
- Compute / Instruction set architecture - the binary 'language' spoken by your hardware (ARM for M1 macs, x86 for most windows PCs) 
- Software package or Library - a 'bundle' of source code / and or pre-compiled binaries
- Dependency - a package used in another piece of code
	- Primary dependency - a package used in your code
	- Transitive dependency - a package used by a package you used...
- Package manager - installs software packages on your system
- Environment manager - lets you install different sets of packages in different places, maybe with specific versions
- System package/library - a package install on your operating system rather than by a particular language
- Container or Virtual machine - isolated environment that functions analogously to a whole computer
- Workflow / Pipeline manager - a tools which manages the execution of multi-step computational analyses
:::

:::

# Quick Overview

::: columns
::: {.column width="60%" style="font-size: 70%;"}
> Checklists for anyone publishing a research paper that has any analysis code associated with it,
> is developing a software tool which will be used by resarchers,
> or is deploying a web service which will be used by researchers.

**11** part **4** tiered checklists for **4** different types of software output

**Types:**

-   Generic Tools
    -   Unitary tool / software package
    -   Multi-part workflows / Pipelines
-   Web-based service
-   ***Record of a specific analysis***

Hoping to take these to the [Software Sustainability Institute Collaborations Workshop 13-15th May](https://www.software.ac.uk/workshop/collaborations-workshop-2025-cw25) 

(Submitted proposal for a workshop)

:::
::: {.column width="40%" style="font-size: 70%;"}
**Parts:**

|                                         |
|-------------------|
| 📒 Source control                       |
| © Licencing                             |
| 📖 Documentation                        |
| 🔗 Making Citable                       |
| ✅ Testing                              |
| 🤖 Automation                           |
| 👥 Peer review / Code Review            |
| 📦 Distribution                         |
| 💽 Environment Management / Portability |
| 🌱 Energy Efficiency                    |
| ⚖ Governance, Conduct, & Continuity     |

**Tiers:**

🥉 Bronze (easy), 🥈 Silver, 🥇 Gold, 🏆 Platinum (very hard)

Gamified with a points system 🎮

:::
:::

# The Checklists

[gitlab.com/HDBI/data-management/checklists](https://gitlab.com/HDBI/data-management/checklists)

```{r, eval=TRUE, echo=FALSE, output=FALSE}
slides_name <- "checklists"
qr_svg <- paste0("data/hdbi-logos/", slides_name, ".svg")
slides_url <- paste0("gitlab.com/HDBI/data-management/", slides_name)
if(!file.exists(qr_svg)) {
	qrcode_obj <- qrcode::qr_code(slides_url)
	svg(qr_svg)
	plot(qrcode_obj)
	dev.off()
}
```

![](`r qr_svg`)

# Generic Areas & Motivating Questions

::: {style="font-size: 60%;"}
|                                         |                                                                                            |
|----------------------|------------------------------------------------------|
| 📒 **Source control**                       | *How can you keep track of the history of your project and collaborate on it?*             |
| © **Licencing**                             | *On what terms can others use your code, and how can you communicate this?*                |
| 📖 **Documentation**                        | *How do people know what your project is, how to use it and how to contribute?*            |
| 🔗 **Making Citable**                       | *How should people make reference to your project and credit your work?*                   |
| ✅ **Testing**                              | *How can you test your project so you can be confident it does what you think it does?*    |
| 🤖 **Automation**                           | *What tasks can you automate to increase consistency and reduce manual work?*              |
| 👥 **Peer review / Code Review**            | *How can you get third party endorsement of and expert feedback on your project?*          |
| 📦 **Distribution**                         | *How can people install or access the software emerging from your project?*                |
| 💽 **Environment Management / Portability** | *How can people get specific versions of your software running on their systems?*          |
| 🌱 **Energy Efficiency**                    | *How can you and your users minimise wasted energy?*                                       |
| ⚖ **Governance, Conduct, & Continuity**     | *How can you be excellent to each other, make good decisions well, and continue to do so?* |
:::

# Focusing today on:

::: {style="font-size: 70%;"}
> **Record of a specific analysis**
> 
> Considerations for publishing code which runs a specific analysis that underpins some result to be published in the academic literature.
>
> The emphasis here is on making the work narrowly reproducible i.e. the analysis of the same data can produce the same result when it is re-run.
> This is a starting point for making results robust (different analysis, same data) and replicable (same analysis, different data), and ultimately generalisable (different analysis, different data).
>
> The other emphasis is on making the work 'verifiable', exposing the complete step-wise detail of the reasoning underpinning the analysis so that it can be scrutinised and understood.
:::

# 📒 Source control

*How can you keep track of the history of your project and collaborate on it?*

::: {style="font-size: 70%;"}

- 'git history' as part of the 'lab notebook' for the comuptational project
	- Temporal sequence
	- Curated but sometimes complex and non-linear
- Ecosystem of tooling & services around git and git hosting that enable other checklist items
	- Distribution, automations for testing, making citable & documentation, platforms for collaboration including review
- 🏆 Advanced uses
	- Commits can be cryptographically signed and timestamped
		- Allows for strong guarantees of provenance for code and data (via git [lfs](https://git-lfs.com/) / [annex](https://git-annex.branchable.com/))
:::

:::notes

https://github.com/opentimestamps/opentimestamps-client/blob/master/doc/git-integration.md

https://medium.com/swlh/git-as-cryptographically-tamperproof-file-archive-using-chained-rfc3161-timestamps-ad15836b883
:::

## 📒 Source control - Checkbox items

::: {style="font-size: 70%;"}
- [ ] Uses git (or other source control tool)
	- [ ]  	🥉Bronze *(Easy)*: Using version control but has a shallow project history, just placed in git for distribution
	- [ ]  	🥈Silver *(Intermediate)*: Longer project history, commit messages of mixed quality, some large messy changes
	- [ ]  	🥇Gold *(Hard)*: Silver plus - Well written commit messages, nice granular commits making discrete self-contained changes. Tags, releases, or branches at major project milestones, maybe some contributions from other users
	- [ ]  	🏆Platinum *(MAXIMUM OVERKILL)*: Gold plus - Some from: [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/); Clean history with a consistent rebasing/merging strategy; Signed commits from all contributors; Contributions go through a consistent workflow like, issues, then a pull request from a branch.
:::

# © Licencing

*On what terms can others use your code, and how can you communicate this?*

::: {style="font-size: 70%;"}
- Must explicitly allow others to use your code if you don't want them to potentially be liable for copyright infringement
	- Similar to open access for a manuscript
	- without a license code posted publicly is a type of 'source available' code, but not 'open source'
- Types of open license:
	- Permissive - *mostly* do whatever you want with it
	- Copy left - heritably open, derived works must also be open
- May need to license your figures and prose with a creative commons license
:::

:::notes
Permissive likely most convenient for small chunks of one-off code: MIT

copy left (e.g. GPL) is good for core libraries where you want to encourage future development to stay open.
GPL can be seen as a problem for comercial use as it can be inconvenient to distribute GPL code alongside proprietary code.
LGPL is a good alternative libraries if you wanted to facilitate 

use over a network: AGPL

'source available' - for example: Code is available, publicily or on request,
but a license/contract places restrictions on how it can be used and shared.
:::

## © Licencing - checkbox items

::: {style="font-size: 70%;"}
- [ ] Project is suitably licensed
	- [ ]  	🥉Bronze *(easy)*: There is a LICENSE file in the repository for a license which meets one of the [OSI](https://opensource.org/osd), [Debian](https://www.debian.org/intro/free), or [FSF/GNU](https://www.gnu.org/philosophy/free-sw.en.html) definitions of free/libre or open source software. Or for any contents that are not software a [Creative Commons](https://creativecommons.org/) license.
	- [ ]  	🥈Silver *(easy)*: If any prose/documentation or images are licenced differently from the code in the project this is indicated and those licences provided.
	If licences have an attribution requirement there is easy to copy text/links for appropriate attribution.
	- [ ]  	🥇Gold *(intermediate)*: Uses [Software Package Data Exchange (SPDX)](https://spdx.org/licenses/) license identifiers for every file/suitable unit of code.
	With a tool such as [REUSE.software](https://reuse.software/) to automate and standardise the process.
	- [ ]  	🏆Platinum *(intermediate)*: all previous tiers plus any images have licensing information embedded in their metadata.
:::

# 📖 Documentation

*How do people know what your project is, how to use it and how to contribute?*

::: {style="font-size: 70%;"}
- Clear documentation is ***critical*** for reproducibility of a one off analysis
- Important to support peer / code review - clear enough for someone other than you to run your analysis
- Usage
	- Environment
		- Dependencies - libraries you've used in your code (versioned)
		- file layout - where your code expects everything to be
	- Installations
		- how to get your code and its dependencies ready to run
	- Run
		- What steps to execute in what order with what commands, flags, inputs and outputs
		- Format / Structure expectations for inputs / outputs
	- Modify
		- Details such as project structure, enough that someone else can understand it, pick it up and usefully alter the code
- Why & How?
	- Explanations of the problem the code tackles and details of how
:::

:::notes
Why & How important component of the (supplementary) methods that an analysis code repo can be to a manuscript

Input / Output expectations documentation is often overlooked.
It is easy to forget to spell this when you've been working with your data / code for a long time.
What assumptions does your code make about its I/O
Example data also really helps with this, lack of example data and I/O documentation make it very hard to use.
:::

## 📖 Documentation - checkbox items

::: {style="font-size: 70%;"}
- [ ] Project has suitable documentation 
	- [ ] 	🥉Bronze *(easy)*: Project has README file and:
		- [ ] README provides a description of the project structure so that the user knows which directories to find things in, possibly including a visual representation of the structure
		- [ ] README contains instructions with sufficient detail for someone else to re-run the analysis
	- [ ]  	🥈Silver *(intermediate)*: Documentation / comments explain WHY things are done in the code
	- [ ]  	🥇Gold *(intermediate)*: You have a simple worked example of your analysis methods with example data to illustrate its soundness in a simple case
	- [ ]  	🏆Platinum *(MAXIMUM OVERKILL)*: The authors manuscript of you paper is a literate programming artefact with numbers, tables and figures programaticaly generated and built in a reproducible computational environment.
			Additional documentation of details not suitable for the main manuscript are in the supplementary material also as literate programming artefacts, more lengthy and complex elements of the analysis might be run as a pipeline or scripts then read from for inclusion in the manuscript.
			Building the manuscript (without caching) reruns the complete analysis and produces the same output (you may need to set random seeds for any analysis that makes use of pseudorandom number generation).
:::

:::notes

:::

# 🔗 Making Citable

*How should people make reference to your project and credit your work?*

::: {style="font-size: 70%;"}
- Providing metadata readable by reference managers 
	- Include a [CITATION.cff](https://citation-file-format.github.io/) in your repository

	````yaml
	authors:
	  - family-names: Druskat
	    given-names: Stephan
	    orcid: https://orcid.org/1234-5678-9101-1121
	title: "My Research Software"
	version: 2.0.4
	````

- Minting a **unique**, **persistent**, **resolvable** identifier
	- Zenodo for DOIs (suitable for one off analyses)
	- software heritage for SWHIDs (arguably better for packages - commit level granularity)
	- Use in the manuscript, bench protocol, and dataset metadata

- Opportunity to emphasise credit for this part of the work that is due to any technical specialists that worked on it

:::

:::notes
:::

## 🔗 Making Citable - checkbox items

::: {style="font-size: 70%;"}
- [ ] Record is Citable
	- [ ]  	🥉Bronze *(easy)*: A [CITATION.cff](https://citation-file-format.github.io/) file exists in the code repository to provide citational metadata about your project
	- [ ]  	🥈Silver *(easy)*: bronze plus the project has persistent resolvable identifier such as a DOI or SWHID, with which it can be referenced, which has been minted for the project using a tool like [zenodo](https://zenodo.org/) or [Software Heritage's Archive](https://www.softwareheritage.org/) to store an archival copy of the project.
	- [ ]  	🥇Gold *(intermediate)*: silver plus:
		- [ ] Contributions are credited using a suitable contributor roles ontology or taxonomy (CROT) such as [CrediT](https://credit.niso.org/), [ScoRo](http://www.sparontologies.net/ontologies/scoro), [CRO](https://github.com/data2health/contributor-role-ontology), or [TaDiRAH](https://tadirah.info/).
		- [ ] All contributors are identified by their [ORCID](https://orcid.org/) or other suitable persistent identifier
	- [ ]  	🏆Platinum *(mixed)*: gold plus any two or more from:
		- [ ] (easy) All research institutions are identified by their [ROR](https://ror.org/) ID
		- [ ] (intermediate) Versioned persistent identifier with automation to update snapshots on zenodo or similar tool when a new version is created.
		- [ ] (intermediate) Annotating work cited in this work with the [Citation Typing Ontology (CiTO)](http://purl.org/spar/cito)
		- [ ] (hard) Your environment is defined with Nix or Guix - this might not seem like it contributes to making software more citable see details below for why this is the case.
:::

# 👥 Peer review / Code Review

*How can you get third party endorsement of and expert feedback on your project?*

::: {style="font-size: 70%;"}
- Simple check - can someone else you know install and run your code from your repo using only the included instructions?
- Slightly more formal external validation someone else can run your code
	- [CODECHECK](https://codecheck.org.uk/)
	- [ReproHack](https://www.reprohack.org/)

- More complete / academic code peer review e.g. from rOpenSci/pyOpenSci, JOSS is typically limited to software packages, though should in theory also form part of the regular peer review process
:::

## 👥 Peer review / Code Review - checkbox items

::: {style="font-size: 70%;"}
- [ ] Code has been subject to a review indicating that someone else could re-run the analysis
	- [ ]  	🥉Bronze *(easy)*: Someone other than you has checked over your project, given you feedback and told you they are reasonably confident they could re-run your analysis without your help.
	- [ ]  	🥈Silver *(easy)*: Someone other than you has scessfully re-run your analysis using only your documentation, (preferably in a different compute environment, such as a different computer/compute cluster)
	- [ ]  	🥇Gold *(intermediate)*: You have a review from [CODECHECK](https://codecheck.org.uk/), [ReproHack](https://www.reprohack.org/) or equivalent and have incorporated suggestions for improving reproducibility from these reviews.
	- [ ]  	🏆Platinum *(intermediate)*: You have reviews which go beyond checking the ability to re-run your code but which also review it's technical correctness
:::


# 💽 Environment Management / Portability

*How can people get specific versions of your software running on their systems?*

::: {style="font-size: 70%;"}
- List of software dependencies and their versions / dependencies
	- Ideally in a format which allows their automated install
	- Wherever possible use cross-platform tools for this (Containers, Nix)
- No hard coded absolute paths, paramaterise Input / Output paths and document defaults
	- ❌ `/home/richardjacton/project/data/sample01.csv`
	- ✅ `./data/sample01.csv` relative to project root
- Is it possible to package all or part of your project to make it easier to install?
:::

## 💽 Environment Management / Portability - Checklist Items

::: {style="font-size: 70%;"}
- [ ] Computational environment description provided
	- [ ]  	🥉Bronze *(Easy)*: List of package versions, e.g. output of `sessionInfo()` in R, not in a machine readable format
	- [ ]  	🥈Silver *(Intermediate)*: Structured language specific environment decription, language environment can be re-created e.g. `renv.lock` in a mostly automated fashion
	- [ ]  	🥇Gold *(Hard)*: Structured full environment description, automated ability to recreate the complete environment including system dependencies
	- [ ]  	🏆Platinum *(MAXIMUM OVERKILL)*: Your description allows the automated bootstrap of the entire* depencency tree of your environment from source with bitwise binary reproducibility (currently almost impossible to achieve, basically only approachable in Guix)
:::

# ⚖ Governance, Conduct, & Continuity

*How can you be excellent to each other, make good decisions well, and continue to do so?*

::: {style="font-size: 70%;"}
- Continuity
	- What happens to your code when you have left the lab?
	- Who can pick up where you left off, made and needed updates?
	- For example: [Rugg-Gunn-lab GitHub Organisation](https://github.com/Rugg-Gunn-Lab)
- Governance 
	- Who has the admin rights on the GitHub Org
- Conduct
	- Ground rules for using it
	- What should be put here, when, and by who?
:::
 
## ⚖ Governance, Conduct, & Continuity - Checkbox Items

::: {style="font-size: 70%;"}
- [ ] The project has a suitable governance model
	- [ ]  	🥉Bronze *(easy)*: The governance model is clearly communicated
	- [ ]  	🥈Silver *(easy)*: Bronze Plus - Project has continuity planning in place (2 or more from)
		- [ ] Source archived and/or mirrored to other platforms
		- [ ] Public archives of key project governance documentation and plans for continuity of operations in the events such as the loss of key project infrastructure
		- [ ] Plans of action in the event project admin(s) are no longer available
	- [ ]  	🥇Gold *(intermediate)*: Project has a governance model appropriate to its scale and goals
		- [ ] Project has clear and transparent processes
	- [ ]  	🏆Platinum *(Hard)*: Project has a track record of good governance and policy, any from:
		- [ ] Decisions have involved the appropriate person(s) and been well documented
		- [ ] Disputes are largely resolved in a respectful and amicable fashion
		- [ ] The project leadership has learned from any mistakes and implemented policy changes as a result
:::

# Points & Medals System

::: columns
::: {.column width="60%" style="font-size: 70%;"}
- You must get at least bronze in all areas to get a bronze medal overall
	- Bronze across the board - Highly attainable
- You get 1 point for a bronze and 4 for a platinum
- After bronze your overall medal tier is the mean, rounded down to the nearest integer of your other scores
	- Platinum across across the board - practically impossible
:::
::: {.column width="20%" style="font-size: 70%; text-align: right"}
**Repo Badges**

- ![](https://img.shields.io/badge/RSSPDC_%F0%9F%A5%89_(12)-Specific_Record-%231b9e77?link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists&link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists%2F-%2Fblob%2Fmaster%2Fchecklists%2Fspecific-record.md)
- ![](https://img.shields.io/badge/RSSPDC_%F0%9F%A5%88_(23)-Pipeline-%23d95f02?link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists&link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists%2F-%2Fblob%2Fmaster%2Fchecklists%2Fsoftware-packages.md)
- ![](https://img.shields.io/badge/RSSPDC_%F0%9F%A5%87_(34)-Software_Package-%23e7298a?link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists&link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists%2F-%2Fblob%2Fmaster%2Fchecklists%2Fsoftware-packages.md)
- ![](https://img.shields.io/badge/RSSPDC_%F0%9F%8F%86_(44)-Web_Service-%237570b3?link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists&link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists%2F-%2Fblob%2Fmaster%2Fchecklists%2Fweb-based-service.md)
- ![](https://img.shields.io/badge/RSSPDC_(4)-Web_Service-%237570b3?link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists&link=https%3A%2F%2Fgitlab.com%2FHDBI%2Fdata-management%2Fchecklists%2F-%2Fblob%2Fmaster%2Fchecklists%2Fweb-based-service.md)

:::
:::

::: {style="font-size: 60%;"}
|                                         | | | | | | | | | |
|----------------------|-|-|-|-|-|-|-|-|
| 📒 **Source control**                       | 1                | 1                | 4                | 1                | 2                | 3                | 3                | 4                |
| © **Licencing**                             | 1                | 1                | 1                | 1                | 2                | 4                | 3                | 4                |
| 📖 **Documentation**                        | 1                | 1                | 1                | 1                | 2                | 3                | 3                | 4                |
| 🔗 **Making Citable**                       | 1                | 1                | 4                | 1                | 2                | 4                | 3                | 4                |
| ✅ **Testing**                              | 1                | 2                | 1                | 1                | 2                | 2                | 3                | 4                |
| 🤖 **Automation**                           | 1                | 1                | 1                | 1                | 2                | 1                | 3                | 4                |
| 👥 **Peer review / Code Review**            | 1                | 1                | 4                | 1                | 2                | 3                | 3                | 4                |
| 📦 **Distribution**                         | 1                | 1                | 1                | 1                | 2                | 3                | 3                | 4                |
| 💽 **Environment Management / Portability** | 1                | 1                | 1                | 1                | 2                | 2                | 3                | 4                |
| 🌱 **Energy Efficiency**                    | 1                | 1                | 4                | 1                | 2                | 3                | 3                | 4                |
| ⚖ **Governance, Conduct, & Continuity**     | **0**            | **0**            | **0**            | 1                | 2                | 4                | 3                | 4                |
| Total Score                                 | 10               | 11               | 22               | 11               | 22               | 32               | 33               | 44               |
| Scaled Score (`floor(total score / 11)`)    | `r floor(10/11)` | `r floor(11/11)` | `r floor(22/11)` | `r floor(11/11)` | `r floor(22/11)` | `r floor(32/11)` | `r floor(33/11)` | `r floor(44/11)` |
| Overall Medal                               | NA               | NA               | NA               | 🥉               | 🥈               | 🥈               | 🥇               | 🏆               |


:::
<!--
# Tiered Checks

::: columns
::: {.column width="30%"}
-   [ ] 🥉 Bronze (easy)
-   [ ] 🥈 Silver
-   [ ] 🥇 Gold
-   [ ] 🏆 Platinum (very hard)
:::

::: {.column width="70%"}
-   Bronze across the board - Highly attainable
-   Platinum across across the board - You're overdoing it
:::
:::
-->


# This Presentation

::: columns
::: {.column width="60%"}
```{r, eval=TRUE, echo=FALSE, output=FALSE}
slides_name <- "epigenetics-meeting-software-checklists_2025-02-12"
qr_svg <- paste0("data/hdbi-logos/", slides_name, ".svg")
slides_url <- paste0("https://hdbi.gitlab.io/data-management/hdbi-data-outputs-quarto-presentations/", slides_name, ".html")
if(!file.exists(qr_svg)) {
	qrcode_obj <- qrcode::qr_code(slides_url)
	svg(qr_svg)
	plot(qrcode_obj)
	dev.off()
}

```
[`r slides_url`](`r slides_url`)

**Questions / Feedback ?**

[Richard.Acton\@babraham.ac.uk](mailto:Richard.Acton@babraham.ac.uk)

:::

::: {.column width="40%"}

![](`r qr_svg`)

:::
:::


::: notes
:::

# References

::: {#refs}
:::

::: notes
:::

# Extra Slides {.appendix visibility="uncounted"}

# 🤖 Automation {visibility="uncounted"} 

*What tasks can you automate to increase consistency and reduce manual work?*

::: {style="font-size: 70%;"}

- Computational Notebooks
- pipeline managers / Make-like systems
- automated environment management tool such as renv, poetry, conda, Nix, or GUIX
- git hosts also tend to have continuous integration and deployment (CI/CD) systems
	- Automatically do things when you 'push' changes to your code
	- For example build these slides and serve them on a website
	- Run tests, or if the analysis is small can run the whole thing
	- Great for checking environment and portability are properly described
- Linters and/or stylers to format code
	- git hooks

:::

:::notes
e.g. These slides
git hooks for things like automatic formatting
:::

## 🤖 Automation - Checkbox Items {visibility="uncounted"} 

::: {style="font-size: 70%;"}
- [ ] Suitable automations are in place
	- [ ]  	🥉Bronze *(easy)*: 1 from this list of processes are automated
		- use of an environment management tool
		- use of a literate programming / computational notebook
		- use of a pipeline manger or make-like tool
		- use of a linter / formatter
		- use of continious integration / continious deployment
		- use of git hooks
		- automated minting of new persistent identifiers on release tagging
		- ...
	- [ ]  	🥈Silver *(easy)*: 2-3 from the above list of processes are automated
	- [ ]  	🥇Gold *(intermediate)*: 4+ from the above list of processes are automated
	- [ ]  	🏆Platinum *(hard)*: note that difficuly is somewhat project dependent
		Your manuscript and its supplements are generated and served on a website after being built from your CI/CD pipeline.
		All statistics and data visualisations in your manuscript are generated programatically by your analysis pipeline from your raw data in CI/CD.
		Results are cached such that if you, for example, change the formatting of a graph only the plotting and rendering code needs to be re-run, but if you change the data the entire pipeline is re-rerun.
:::

# ✅ Testing {visibility="uncounted"} 

*How can you test your project so you can be confident it does what you think it does?*

::: {style="font-size: 70%;"}

- For one off analyses automated unit testing frameworks for software packages can be:
	- a bit overkill, though helpful to know for quick tests of core logic
	- Not best suited to the sorts of 'sanity' tests it is good to do

- Dummy data
	- Simulate inputs which you expect to have certain outputs
- 'fuzz' testing 
	- noisy random inputs, 'graceful failure' when inputs don't make sense over potentially misleading outputs
- 'unit' testing
	- Check core logic is correct

- Particularly useful for pre-registered analyses where you plan and test the analysis prior to getting the primary dataset
:::

:::notes
fuzz testing - if you give it insane inputs does it complain or does it spit out something that might plausibly have come from a sane input.
code to catch and complain about insane inputs helps you avoid embarassing mistakes
Depending on your data type you may find that tools to simulate date of that type may already exist

pre-registered analyses still need to be public even with timestamped commits as you could have created multiple analyses prior to publication and cherry-picked the one with results you like to reveal.
:::

## ✅ Testing - Checkbox Items {visibility="uncounted"} 

::: {style="font-size: 70%;"}
- [ ] Project has undergone suitable testing
	- [ ]  	🥉Bronze *(easy)*: Includes a minimal test data set necessary to demonstrate the basic functionality of the analysis
	- [ ]  	🥈Silver *(easy)*: Includes test datasets which cover a range of outcomes of the analysis
	- [ ]  	🥇Gold *(intermediate)*: You are using unit tests and an automated testing framework to check the correctness of core steps of your analysis
	- [ ]  	🏆Platinum *(hard)*: Have your analysis code written and tested on preliminary or simulated data in advance of recieving your principle dataset with a copy of your code from this time archived and referenced in a pre-registration or registered report.
:::

# 🌱 Energy Efficiency {visibility="uncounted"} 

*How can you and your users minimise wasted energy?*

::: {style="font-size: 70%;"}
- less relevant for records of specific analyses than whole analyses which will be re-run many times
	- Consider profiling and refactoring any parts of your code which you will re-run many times
- Minimise unnecessary materials to archive
	- Favour Succinct efficient representations of environments and test data
:::

## 🌱 Energy Efficiency - Checkbox Items {visibility="uncounted"} 

::: {style="font-size: 70%;"}
- [ ] Consideration has been given to the energy efficiency of the code
	- [ ]  	🥉Bronze *(easy)*: minimise unnecessary output files
	- [ ]  	🥈Silver *(easy)*: bronze plus: Profile your code and refactor inefficient parts
	- [ ]  	🥇Gold *(intermediate)*: silver plus: Estimate and share the carbon footprint of your computations with a tools such as [green algorithms calculator](http://calculator.green-algorithms.org/)
	- [ ]  	🏆Platinum *(intermediate)*: gold plus: Offload suitable computations to hardware accelerators where possible
:::

# 📦 Distribution {visibility="uncounted"} 

::: {style="font-size: 70%;"}
*How can people install or access the software emerging from your project?*

- Deposit the code publicily (a suitable license)
	- Use a suitable format for code distribution e.g. a git repo
		- No PDFs/Word Docs, plain text files are better than this but not as good as git
- Make it discoverable
	- Reference it in appropriate places like the metadata for any datasets you used, your protocols, your manuscript
	- keywords, author information, etc to make searchable
	- Archive in searchable repsoitories like Zenodo
- Use standard formats for any packaging of the software that you do
- Use an environment management tool to record and facilitate installing dependencies of the correct versions and distribut this alongside the code
- Share not just the code but the inputs, outputs, and environment in which it ran in a way others can easily copy modify and re-run
:::

:::notes
not just the software here but its outputs
:::

## 📦 Distribution - Checkbox Items {visibility="uncounted"} 

::: {style="font-size: 70%;"}
- [ ] Project is distributed in a suitable fashion
	- [ ]  	🥉Bronze *(Easy)*: Code and data (barring privacy related access restrictions) are in public repositories.
	- [ ]  	🥈Silver *(Intermediate)*: Detailed instructions on how to fetch, install and configure the tools and data needed re-run your analysis, and how to re-run the analysis in the described environment.
	- [ ]  	🥇Gold *(Intermediate)*: Project is in a reproducible interactive environment such as those offered by [binder](https://binderhub.readthedocs.io/en/latest/index.html) or [renku](https://renkulab.io/).
	- [ ]  	🏆Platinum *(Hard)*: Gold plus - Your project is built and served as a website using continuous integration and deployment tools such that your analysis is run on your data in a reproducible compute environment and computational results like graphs and statistics are programatically inserted into your output. (It is best to have some form of caching when doing this).
:::

